export class CreateEmployeeDto {
  name: string;

  role: string;

  phone: string;

  salary: number;
}
